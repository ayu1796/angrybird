﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowBird : Bird
{
    // Start is called before the first frame update
    [SerializeField]
    public float _boostForce = 100;
    public bool _hasBoost = false;

    void Start()
    {
        RigidBody.bodyType = RigidbodyType2D.Kinematic;
        Collider.enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Boost()
    {
        if (State == BirdState.Thrown && !_hasBoost)
        {
            RigidBody.AddForce(RigidBody.velocity * _boostForce);
            _hasBoost = true;
        }
    }

    public override void onTap()
    {
        Boost();

    }
}
